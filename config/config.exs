# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :unu,
  ecto_repos: [Unu.Repo]

# Configures the endpoint
config :unu, Unu.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "NvxMKefHVbx/XZu1jTYuc/V59F+3hNSjmHNrO5KlfaPu+y2GzFJeomUE/qiMyDH+",
  render_errors: [view: Unu.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Unu.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Configure phoenix generators
config :phoenix, :generators,
  binary_id: true

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
