defmodule Unu.UserController do
  use Unu.Web, :controller

  alias Unu.User

  ## For interactive testing only:

  def index_page(conn, _params) do
    users = Repo.all(User)
    render(conn, "index.html", users: users)
  end

  def delete(conn, %{"id" => id}) do
    user = Repo.get!(User, id) |> Repo.preload(talent_profile: :head_shots, talent_profile: :genres)
    %User{talent_profile: %Unu.TalentProfile{head_shots: head_shots,
					     genres: genres} = talent_profile} = user
    Enum.each(head_shots, &Repo.delete(&1))
    Enum.each(genres, &Repo.delete(&1))
    Repo.delete(talent_profile)
    Repo.delete(user)
    conn
    |> put_flash(:info, "User deleted!")
    |> redirect(to: user_path(conn, :index_page))
  end
end
