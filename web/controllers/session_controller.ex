defmodule Unu.SessionController do
  use Unu.Web, :controller

  def login_page(conn, _params) do
    render(conn, "login.html", layout: {Unu.LayoutView, "talent_app.html"})
  end

  def login(conn, %{"login" => %{"email" => email}}) do
    case Repo.get_by(Unu.User, email: email) do
      nil ->
	conn
	|> put_flash(:error, "Unrecognized email or password.")
	|> render("login.html", layout: {Unu.LayoutView, "talent_app.html"})
      user ->
	conn
	|> Unu.Auth.login(user)
	|> redirect(to: talent_path(conn, :edit_page, user))
    end
  end

  def logout(conn, _params) do
    conn
    |> Unu.Auth.logout()
    |> redirect(to: page_path(conn, :landing_page))
  end
end
