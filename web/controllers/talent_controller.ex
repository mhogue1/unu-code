defmodule Unu.TalentController do
  use Unu.Web, :controller

  import Ecto.Changeset

  def landing_page(conn, _params) do
    render(conn, "landing.html", layout: {Unu.LayoutView, "talent_app.html"})
  end
  
  def signup_page(conn, _params) do
    render(conn, "signup.html", changeset: ui_changeset(%Unu.TalentViewModel{}, %{}),
      layout: {Unu.LayoutView, "talent_app.html"})
  end

  def signup_post(conn, %{"params" => ui_params}) do
    ui_params = save_head_shot(ui_params)
    case Repo.insert(db_changeset(%Unu.User{}, ui_params)) do
      {:ok, db_struct} ->
        conn
    	|> Unu.Auth.login(db_struct)
        |> put_flash(:info, "You're signed up.")
        |> redirect(to: talent_path(conn, :edit_profile_page, db_struct))
      {:error, db_changeset} ->
	ui_changeset =
	  ui_changeset(%Unu.TalentViewModel{}, ui_params) |> errors_from(db_changeset)
        render(conn, "signup.html", changeset: %{ui_changeset | action: :insert},
	  layout: {Unu.LayoutView, "talent_app.html"})
    end
  end

  def show_page(conn, %{"id" => id}) do
    render(conn, "show.html", talent: query(id) |> db_to_ui_struct())
  end

  def edit_profile_page(conn, %{"id" => id}) do
    ui_struct = query(id) |> db_to_ui_struct()
    render(conn, "edit_profile.html", user_id: id, talent: ui_struct,
      changeset: ui_changeset(ui_struct, %{}),
      layout: {Unu.LayoutView, "talent_app.html"})
  end

  def upload_headshot_post(conn, %{"id" => user_id} = ui_params) do
    # try do
      ui_params = save_head_shot(ui_params)
      %{"head_shot_filename" => filename} = ui_params
      talent_profile = Repo.get_by!(Unu.TalentProfile, user_id: user_id)
      changeset = Ecto.Changeset.change(talent_profile, head_shot_filename: filename)
      talent_profile = Repo.update!(changeset)
      talent_profile = Repo.preload(talent_profile, :head_shots)
      head_shot = Ecto.build_assoc(talent_profile, :head_shots, filename: filename)
      Repo.insert!(head_shot)
      json(conn, %{"filename" => filename})
    # rescue
    #   e -> json(put_status(conn, 500), e)
    # end
    
    # Repo.get!(Unu.User, user_id)
    # case Repo.update(db_changeset(query(user_id), ui_params)) do
    #   {:ok, db_struct} -> json(conn, db_struct)
    #   {:error, db_changeset} -> json(put_status(conn, 500), db_changeset)
    # end

    # ui_struct = query(id) |> db_to_ui_struct()
    # render(conn, "edit_profile.html", user_id: id, talent: ui_struct,
    #   layout: {Unu.LayoutView, "talent_app.html"})
  end

  def edit_post(conn, %{"id" => id, "params" => ui_params}) do
    ui_params = save_head_shot(ui_params)
    case Repo.update(db_changeset(query(id), ui_params)) do
      {:ok, _db_struct} ->
        conn
        |> put_flash(:info, "Your profile has been updated.")
        |> redirect(to: talent_path(conn, :show_page, id))
      {:error, db_changeset} ->
	ui_changeset =
	  ui_changeset(%Unu.TalentViewModel{}, ui_params) |> errors_from(db_changeset)
	render(conn, "edit_profile.html", user_id: id, changeset: %{ui_changeset | action: :update},
	  layout: {Unu.LayoutView, "talent_app.html"})
    end
  end

  defp ui_changeset(%Unu.TalentViewModel{} = ui_struct, %{} = ui_params), do:
  cast(ui_struct, ui_params,
    [:first_name, :last_name, :email, :auth,
     :talent_profile_id, :head_shot_filename, :bio, :primary_instrument,
     :career_start])

  defp db_changeset(%Unu.User{} = db_struct, %{} = ui_params) do
    db_params = ui_to_db_params(db_struct, ui_params)
    cast(db_struct, db_params, [:first_name, :last_name, :email, :auth])
    |> validate_required([:first_name, :last_name, :email, :auth])
    |> unique_constraint(:email)
    |> cast_assoc(:talent_profile, with: &db_changeset_talent_profile/2)
  end

  defp db_changeset_talent_profile(db_struct, db_params), do:
  cast(db_struct, db_params, [:id, :head_shot_filename, :bio, :primary_instrument,
			      :career_start])
  |> validate_required([:bio])
  |> cast_assoc(:head_shots, with: &db_changeset_headshots/2)
  |> cast_assoc(:genres, with: &db_changeset_genres/2)

  defp db_changeset_headshots(db_struct, db_params), do:
  cast(db_struct, db_params, [:id, :filename])

  defp db_changeset_genres(db_struct, db_params), do:
  cast(db_struct, db_params, [:id, :genre])

  defp query(id), do: Repo.get!(Unu.User, id) |> Repo.preload([talent_profile: :head_shots, talent_profile: :genres])

  defp diff_headshots(db_params, %Unu.User{talent_profile: %Unu.TalentProfile{head_shots: db_head_shots}}, %{"head_shot_filename" => new_filename}) do
    current_head_shots = Enum.map(db_head_shots, &Map.from_struct(&1))
    Map.put(db_params, "head_shots", [%{"filename" => new_filename} | current_head_shots])
  end

  defp diff_headshots(db_params, _db_struct, %{"head_shot_filename" => new_filename}) do
    Map.put(db_params, "head_shots", [%{"filename" => new_filename}])
  end

  defp diff_headshots(db_params, _db_struct, _ui_params), do: db_params

  defp put_genres(db_params, new_genres), do:
  Map.put(db_params, "genres", Enum.map(new_genres, &(%{"genre" => &1})))

  defp diff_genres(db_params, %Unu.User{talent_profile: %Unu.TalentProfile{genres: db_genres}}, %{"genres" => new_genres}) do
    current_genres = Enum.sort(Enum.map(db_genres, &(&1.genre)))
    new_genres = Enum.sort(new_genres)
    if (current_genres != new_genres) do
      put_genres(db_params, new_genres)
    else
      db_params
    end
  end

  defp diff_genres(db_params, _db_struct, %{"genres" => new_genres}), do:
  put_genres(db_params, new_genres)

  defp diff_genres(db_params, _db_struct, _ui_params), do: db_params

  defp ui_to_db_params(db_struct, ui_params) do
    db_talent_profile_params =
      Map.take(ui_params, ["head_shot_filename", "bio", "primary_instrument", "career_start"])
      |> Map.put("id", Map.get(ui_params, "talent_profile_id"))
      |> diff_headshots(db_struct, ui_params)
      |> diff_genres(db_struct, ui_params)
    Map.put(ui_params, "talent_profile", db_talent_profile_params)
  end

  defp db_to_ui_struct(%Unu.User{talent_profile: %{head_shots: head_shots, genres: genres} = db_talent_profile} = db_user) do
    ui_map =
      Map.from_struct(db_talent_profile)
      |> Map.put(:talent_profile_id, db_talent_profile.id)
      |> Map.put(:head_shots, Enum.map(head_shots, &(&1.filename)))
      |> Map.put(:genres, Enum.map(genres, &(&1.genre)) |> Enum.sort)
      |> Map.merge(Map.from_struct(db_user))
    struct(Unu.TalentViewModel, ui_map)
  end

  defp errors_from(%Ecto.Changeset{} = to_changeset, %Ecto.Changeset{} = from_changeset) do
    traverse_errors(from_changeset, &(&1)) |> copy_errors(to_changeset)
  end

  defp copy_errors(%{} = errors, %Ecto.Changeset{} = to_changeset), do:
  Enum.reduce(errors, to_changeset, &copy_errors/2)

  defp copy_errors({:talent_profile, errors}, %Ecto.Changeset{} = to_changeset), do:
  copy_errors(errors, to_changeset)

  defp copy_errors({field, errors}, %Ecto.Changeset{} = to_changeset), do:
  Enum.reduce(errors, to_changeset, fn {msg, opts}, to_changeset ->
    add_error(to_changeset, field, msg, opts)
  end)

  defp save_head_shot(%{"head_shot" => %Plug.Upload{filename: filename, path: path}} = ui_params) do
    extension = Path.extname(filename) |> String.downcase()
    new_filename = Ecto.UUID.generate() <> extension
    File.copy!(path, Path.join("/media/talent/head_shot", new_filename))
    Map.put(ui_params, "head_shot_filename", new_filename)
  end

  defp save_head_shot(ui_params), do: ui_params
end
