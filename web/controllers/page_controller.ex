defmodule Unu.PageController do
  use Unu.Web, :controller

  def landing_page(conn, _params) do
    render(conn, "landing.html")
  end

  def talent_search(conn, _params) do
    user_email = get_session(conn, :user_email)
    render(conn, "talent_search.html", user_email: user_email)
  end
end
