'use strict';

//import React from 'react';
//import ReactDOM from 'react-dom';
// import socket from "./socket"

let registerEditProfile = (document) => {
  let link = document.getElementById("headshot_link");
  let selector = document.getElementById("headshot_selector");
  link.addEventListener("click", (e) => {
    selector.click();
    e.preventDefault();
  }, false);
};

export var App = {
  upload_headshot: (files) => {
    if (files.length > 0) {
      let talentData = document.getElementById("talent_data");
      let formData = new FormData();
      formData.append("_csrf_token", talentData.getAttribute("data-csrf"));
      formData.append("id", talentData.getAttribute("data-id"));
      formData.append("head_shot", files.item(0));
      let req = new XMLHttpRequest();
      req.open("POST", talentData.getAttribute("data-upload"));
      req.onload = (event) => { window.location.reload(true); }
      req.send(formData);
    }
  },
  register: (template, document) => {
    switch (template) {
    case 'edit_profile.html':
      registerEditProfile(document);
    }
  }
};
