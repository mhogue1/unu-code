defmodule Unu.TalentProfileHeadShot do
  use Unu.Web, :model

  schema "talent_profile_head_shots" do
    belongs_to :talent_profile, Unu.TalentProfile
    field :filename, :string
    timestamps()
  end
end
