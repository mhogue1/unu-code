defmodule Unu.TalentProfile do
  use Unu.Web, :model

  schema "talent_profiles" do
    belongs_to :user, Unu.User
    field :head_shot_filename, :string
    field :bio, :string
    field :primary_instrument, :string
    has_many :head_shots, Unu.TalentProfileHeadShot
    has_many :genres, Unu.TalentProfileGenre, on_replace: :delete
    field :career_start, :string
    timestamps()
  end
end
