defmodule Unu.TalentProfileGenre do
  use Unu.Web, :model

  schema "talent_profile_genres" do
    belongs_to :talent_profile, Unu.TalentProfile
    field :genre, :string
    timestamps()
  end
end
