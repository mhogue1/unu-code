defmodule Unu.User do
  use Unu.Web, :model

  schema "users" do
    field :first_name, :string
    field :last_name, :string
    field :email, :string
    field :auth, :string, default: "trust-me"
    has_one :talent_profile, Unu.TalentProfile
    timestamps()
  end
end
