defmodule Unu.TalentView do
  use Unu.Web, :view

  def headshot(nil) do
    content_tag :div, class: "profile-no-photo" do
      ""
    end
  end

  def headshot(filename) do
    img_tag("/headshot/" <> filename, class: "profile-photo")
  end

  def edit_headshot(nil) do
    content_tag :div, class: "profile-no-photo" do
      "Upload a photo!"
    end
  end

  def edit_headshot(filename) do
      headshot(filename)
      img_tag("/headshot/" <> filename, class: "profile-photo")
      content_tag :span do
	"Upload a New Photo"
      end
  end

  def genres(form) do
    genres(form,
      ["Classical",
       "Early Music",
       "Jazz",
       "Musical Theater",
       "Opera",
       "Romantic",
       "Sacred Music"])
  end

  def genres(form, genres) do
    content_tag :div, class: "form-group" do
      [genres_label(form),
       genres_group(form, genres)]
    end
  end

  def genres_label(form) do
    label form, :genres, "Genre", class: "control-label"
  end

  def genres_group(form, genres) do
    content_tag :div, class: "input-group" do
      content_tag :div, [{:aria, [label: "Genre"]},
			 {:data, [toggle: "buttons"]},
			 class: "btn-group",
			 role: "group",
			 id: input_id(form, :genres)] do
	Enum.map(genres, &genre_option(form, &1))
      end
    end
  end

  def genre_option(form, genre) do
    content_tag :label, class: genre_class(form, genre) do
      content_tag :input,
	type: "checkbox",
	autocomplete: "off",
	name: input_name(form, :genres) <> "[]",
	value: genre,
	checked: genre_selected(form, genre) do
	genre
      end
    end
  end

  def genre_class(form, genre) do
    if genre_selected(form, genre) do
      "btn btn-default active"
    else
      "btn btn-default"
    end
  end

  defp genre_selected(form, genre), do: (input_value(form, :genres) || []) |> Enum.member?(genre)

  def primary_instrument(form) do
    primary_instrument_render(form,
      ["Soprano", "Mezzo-Soprano", "Alto", "Belter", "Tenor", "Baritone", "Bass"])
  end

  defp primary_instrument_render(form, names) do
    content_tag :div, class: "form-group" do
      [primary_instrument_label(form),
       primary_instrument_group(form, names)]
    end
  end

  defp primary_instrument_label(form) do
    label form, :primary_instrument, "Voice Part", class: "control-label"
  end

  defp primary_instrument_group(form, names) do
    content_tag :div, class: "input-group" do
      content_tag :div, [{:aria, [label: "Voice Part"]},
			 {:data, [toggle: "buttons"]},
			 class: "btn-group",
			 role: "group",
			 id: input_id(form, :primary_instrument)] do
	Enum.map(names, &primary_instrument_option(form, &1))
      end
    end
  end

  defp primary_instrument_option(form, name) do
    content_tag :label, class: primary_instrument_class(form, name) do
      content_tag :input,
	type: "radio",
	autocomplete: "off",
	name: input_name(form, :primary_instrument),
	value: name,
	checked: primary_instrument_selected(form, name) do
	name
      end
    end
  end

  defp primary_instrument_class(form, name) do
    if primary_instrument_selected(form, name) do
      "btn btn-default active"
    else
      "btn btn-default"
    end
  end

  defp primary_instrument_selected(form, name) do
    case input_value(form, :primary_instrument) do
      ^name -> true
      _ -> false
    end
  end
end
