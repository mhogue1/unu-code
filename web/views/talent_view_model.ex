defmodule Unu.TalentViewModel do
  use Unu.Web, :model

  embedded_schema do
    field :first_name, :string
    field :last_name, :string
    field :email, :string
    field :auth, :string, default: "trust-me"
    field :talent_profile_id, :binary_id
    field :head_shot_filename, :string
    field :bio, :string
    field :genres, :string
    field :primary_instrument, :string
    field :career_start, :string
  end
end
