defmodule Unu.Router do
  use Unu.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Unu.Auth
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug :fetch_session
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  scope "/", Unu do
    pipe_through :browser

    get  "/", TalentController, :landing_page
    # get  "/", PageController, :landing_page

    get  "/talent/signup", TalentController, :signup_page
    post "/talent/signup", TalentController, :signup_post
    get  "/talent/:id", TalentController, :show_page
    get  "/talent/:id/edit_profile", TalentController, :edit_profile_page
    post "/talent/:id", TalentController, :edit_post

    get  "/login", SessionController, :login_page
    post "/login", SessionController, :login
    post "/logout", SessionController, :logout

    get  "/talent", PageController, :talent_search

    # For interactive testing only:
    get "/users", UserController, :index_page
    delete "/users/:id", UserController, :delete
  end

  # Other scopes may use custom stacks.
  scope "/api", Unu do
    pipe_through :api

    post "/talent/:id/upload_headshot", TalentController, :upload_headshot_post
  end
end
