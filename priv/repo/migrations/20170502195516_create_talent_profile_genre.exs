defmodule Unu.Repo.Migrations.CreateTalentProfileGenre do
  use Ecto.Migration

  def change do
    create table(:talent_profile_genres, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :talent_profile_id, references(:talent_profiles, type: :binary_id)
      add :genre, :text
      timestamps()
    end
  end
end
