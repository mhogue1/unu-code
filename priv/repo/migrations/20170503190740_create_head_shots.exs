defmodule Unu.Repo.Migrations.CreateHeadShots do
  use Ecto.Migration

  def change do
    create table(:talent_profile_head_shots, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :talent_profile_id, references(:talent_profiles, type: :binary_id)
      add :filename, :text
      timestamps()
    end
  end
end
