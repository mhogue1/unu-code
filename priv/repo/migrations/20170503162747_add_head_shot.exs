defmodule Unu.Repo.Migrations.AddHeadShot do
  use Ecto.Migration

  def change do
    alter table(:talent_profiles) do
      add :head_shot_filename, :text
    end
  end
end
