defmodule Unu.Repo.Migrations.CreateTalentProfile do
  use Ecto.Migration

  def up do
    create table(:talent_profiles, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :user_id, references(:users, type: :binary_id)
      add :bio, :text
      add :primary_instrument, :text
      timestamps()
    end

    execute "CREATE EXTENSION IF NOT EXISTS pgcrypto;"
    
    execute """
INSERT INTO talent_profiles (id, user_id, bio, primary_instrument, inserted_at, updated_at)
SELECT gen_random_uuid(), id, bio, primary_instrument, inserted_at, updated_at FROM users;
    """

    alter table(:users) do
      remove :bio
      remove :primary_instrument
    end
  end

  def down do
    alter table(:users) do
      add :bio, :text
      add :primary_instrument, :text
    end

    execute """
UPDATE users SET bio = talent_profiles.bio, primary_instrument = talent_profiles.primary_instrument FROM talent_profiles
WHERE users.id = talent_profiles.user_id;
    """

    drop table(:talent_profiles)
  end
end
