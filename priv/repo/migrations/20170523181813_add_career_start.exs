defmodule Unu.Repo.Migrations.AddCareerStart do
  use Ecto.Migration

  def change do
    alter table(:talent_profiles) do
      add :career_start, :text
    end
  end
end
