defmodule Unu.Repo.Migrations.AddPrimaryInstrument do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :primary_instrument, :text
    end
  end
end
