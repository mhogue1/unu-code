defmodule Unu.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    create table(:users, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :first_name, :text
      add :last_name, :text
      add :email, :text
      add :bio, :text
      add :auth, :text

      timestamps()
    end

    create unique_index(:users, [:email])
  end
end
