exports.config = {
  files: {
    javascripts: {
      joinTo: "js/app.js"
    },
    stylesheets: {
      joinTo: "css/app.css",
      order: {
        after: ["web/static/css/app.css"]
      }
    },
  },

  conventions: {
    assets: /^(web\/static\/assets)/
  },

  paths: {
    watched: [
      "web/static",
      "test/static"
    ],
    public: "priv/static"
  },

  plugins: {
    babel: {
      ignore: [/web\/static\/vendor/],
      presets: ['env', 'react']
    },
    sass: {
      options: {
	includePaths: ["node_modules/bootstrap-sass/assets/stylesheets"],
	precision: 8
      }
    }
  },

  npm: {
    globals: {
      $: 'jquery',
      jQuery: 'jquery',
      bootstrap: 'bootstrap-sass'
    }
  },

  modules: {
    autoRequire: {
      "js/app.js": ["web/static/js/app"]
    }
  },

  overrides: {
    production: {
      sourceMaps: true
    }
  }
};
