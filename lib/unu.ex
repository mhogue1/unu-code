defmodule Unu do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec

    children = [
      supervisor(Unu.Repo, []),
      supervisor(Unu.Endpoint, []),
    ]

    opts = [strategy: :one_for_one, name: Unu.Supervisor]
    Supervisor.start_link(children, opts)
  end

  @doc """
  Tell Phoenix to update the endpoint configuration
  whenever the application is updated.
  """
  def config_change(changed, _new, removed) do
    Unu.Endpoint.config_change(changed, removed)
    :ok
  end
end
